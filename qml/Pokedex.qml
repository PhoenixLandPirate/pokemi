/*
 * Copyright (C) 2020  phoenixlandpirate
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Pokedex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

Page {
    id: page
    
                ListView {
        id: mainlistView
        focus: true
        currentIndex: -1
        anchors.fill: parent
        
        delegate: ItemDelegate {
            width: parent.width
            text: model.title
            highlighted: ListView.isCurrentItem
            onClicked: {
                listView.currentIndex = index
                stackView.push(model.source)
            }
        }
        
        model: ListModel {
            id: generation1
            ListElement { title: "001 Bulbasaur"; source: "Gengar.qml" }
            ListElement { title: "002 " }
            ListElement { title: "Generation 3" }
            ListElement { title: "Generation 4" }
        }
        
        ScrollIndicator.vertical: ScrollIndicator { }
    }
}
