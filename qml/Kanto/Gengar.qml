/*
 * Copyright (C) 2020  phoenixlandpirate
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Pokedex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.6
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

Page {
    id: page
    
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        
        Repeater {
            model: 3
            
            Pane {
                width: swipeView.width
                height: swipeView.height
                
                Column {
                    spacing: 20
                    width: parent.width

                    
                Image {
                   source: "Gengar.png"
                   anchors.horizontalCenter: parent.horizontalCenter
                    }
                    
                Label {
                    width: parent.width
                    wrapMode: Label.Wrap
                    horizontalAlignment: Qt.AlignHCenter
                    text: "Type:"
                    }  
                
                RowLayout {
                Button {
                text: "Ghost" 
                        }
                Button {
                    text:"Poison"
                    }
                anchors.horizontalCenter: parent.horizontalCenter

                }
                Label {
                    width: parent.width
                    wrapMode: Label.Wrap
                    horizontalAlignment: Qt.AlignHCenter
                    text: " Hiding in people's shadows at night, it absorbs their heat. The chill it causes makes the victims shake."
                    }
                }
            }
        }
    }

            
            footer: TabBar {
                id: tabBar
                currentIndex: swipeView.currentIndex
                
                    TabButton {
                        text: "About"
                    }
                    TabButton {
                        text: "Moves"
                    }
                    TabButton {
                        text: "Evolution"
                    }
                
            }
}

