/*
 * Copyright (C) 2020  phoenixlandpirate
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Pokedex is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

ApplicationWindow {
    id: window
    width: 600
    minimumWidth: 320
    height: 400
    minimumHeight: 380
    visible: true
    title: "Pokedex by Your pal phoenixlandpirate"
    
header: ToolBar {
 Material.foreground: "white"
    RowLayout {
        spacing:20
        anchors.fill: parent
   
 /// Button on left of ToolBar, it opens the draw, to show pokedex regions
        ToolButton {
            id: generations
            text: qsTr("M")
            width: 120
            onClicked: {
                
                drawer.open()
            }
        }
        

/// This is the title on the ToolBar, it should tell you what page you're on, currently it just tells you that you're using the pokedex app        
        Label {
            text: "Pokedex"
            elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
        }

 /// This the button on the right, it tells a option Menu to open, currently this doesn't do anything important, need to add credits, licenses, etc                            
        ToolButton {
            id: controls
            text: qsTr("⋮")
            width:120
            onClicked: optionsMenu.open()
            
            Menu {
                id: optionsMenu
                x: parent.width - width
                y: parent.height
                transformOrigin: Menu.TopRight
                
                MenuItem {
                        text: "About"
                        onTriggered: aboutDialog.open()
                    }
                }
}
        }
    }


/// This is the actual drawer, this shows you the generations of pokemon via region
Drawer {
    id: drawer
    width: Math.min(window.width, window.height) / 3 * 2
    height: window.height
    dragMargin: 0
    
    ListView {
        id: listView
        focus: true
        currentIndex: -1
        anchors.fill: parent
        
        delegate: ItemDelegate {
            width: parent.width
            text: model.title
            highlighted: ListView.isCurrentItem
            onClicked: {
                listView.currentIndex = index
                stackView.push(model.source)
                drawer.close()
            }
        }
        
        model: ListModel {
            ListElement { title: "Pokedex"; source: "PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "Kanto"; source: "Kanto/Kanto.qml" }
            ListElement { title: "Johto"; source: "PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "Hoenn"; source: "PageUnavailable/Pageunavailable.qml" }
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }
}


    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: Pane {
            id: pane

           Label {
                text: "Gotta catch em all."
                anchors.margins: 20
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                wrapMode: Label.Wrap
            }
        }
    }
    
        Dialog {
        id: aboutDialog
        modal: true
        focus: true
        title: "About"
        x: (window.width - width) / 2
        y: (window.height - height) / 2 - header.height
        width: window.width / 3 * 2
        height: Math.min(implicitHeight, window.height / 3 * 2)
        implicitHeight: aboutColumn.height + header.height + topPadding + bottomPadding
        Flickable {
            id: aboutFlickable
            contentHeight: aboutColumn.height
            anchors.fill: parent
            clip: true
            Column {
                id: aboutColumn
                spacing: 20

                Label {
                    width: aboutDialog.availableWidth
                    text: "This is the start of a Pokedex application, the idea behind the app is to help you on your pokemon journey, to tell you when and how a pokemon evolves, what moves they can learn, and when, and it should be searchable, with the ability to add pokemon to a favourites and a caught list, so you can tick off every pokemon you have."
                    wrapMode: Label.Wrap
                }

                Label {
                    width: aboutDialog.availableWidth
                    text: "This Pokedex has been developed by Phoenix LandPirate, the source code it on gitlab."
                    wrapMode: Label.Wrap
                }
                Label {
                    width: aboutDialog.availableWidth
                    text: "Pokemon Database can be found here "
                    + "https://github.com/decentralion/PokemonSQLTutorial "
                    + "  "
                    + "The Dinosaur imgae can be found here "
                    + "https://publicdomainvectors.org/en/free-clipart/Red-cartoon-dino/78665.html"
                    wrapMode: Label.Wrap
            }
                Label {
                    width: aboutDialog.availableWidth
                    text: "Pokemon, Pokedex, are property of Nintendo, so please no action law suit, or else I might need to sell my Animal Crossing, and no one wants that."
                    wrapMode: Label.Wrap

        }
        }
        }

        }
    
}



    
