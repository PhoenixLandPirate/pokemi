# PokeMi

Pokedex for Yumi's favorite OS, Ubuntu Touch, by UBports, designed to be compatible with Plasma mobile.

## Add Database

Gitlab shouted at me telling me the database is to large, fortunatly, I dont know how to use Databases anyways, so the code doesn't use it, but it should!
So you can find the database somewhere on the internet, it's linked in the Main.qml somewhere, I'll go find it in a minute..


Ah here we are https://github.com/decentralion/PokemonSQLTutorial
just download pokedex.db, place it in "Databases" and pretend that it does something.

good night.
## License

Copyright (C) 2020  phoenixlandpirate

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
