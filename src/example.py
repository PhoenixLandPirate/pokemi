'''
 Copyright (C) 2020  phoenixlandpirate

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 Pokedex is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sqlite3
db = sqlite3.connect(':memory:')
db = sqlite3.connect('pokedex.db')

cursor = db.cursor()
cursor.execute('''SELECT sname.name, pokemon.* FROM pokemon JOIN pokemon_species as species 
		ON pokemon.species_id = species.id
	JOIN pokemon_species_names as sname
		ON sname.pokemon_species_id = species.id
		WHERE species.generation_id == 1
	AND sname.local_language_id == 9''')
#user1 = cursor.fetchone() #retrieve the first row
#print(user1[0]) #Print the first column retrieved(user's name)
all_rows = cursor.fetchall()
for row in all_rows:
    # row[0] returns the first column in the query (name), row[1] returns email column.
    print('{0}: {1}'.format(row[1], row[0] ))
