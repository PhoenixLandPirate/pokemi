 import QtQuick 2.6
import QtQuick.Controls 2.1
/*import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3 */

 Page {
    id: page
    
 ListView {
        id: mainlistView
        focus: true
        currentIndex: -1
        anchors.fill: parent
        spacing: 1
        
            delegate: ItemDelegate {
            width: parent.width
            text: model.title
            
            highlighted: ListView.isCurrentItem
            onClicked: {
                listView.currentIndex = index
                stackView.push(model.source)
        }
            }
                
        
        model: ListModel {
            id: kanto
            ListElement { title: "1:  Bulbasaur"; imageSource: "/Minicons/Bulbico.png"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "2:  Ivysaur"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "3:  Venusaur"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "4: Charmander"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "5: Charmeleon"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "6: Charizard"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "7: Squirtle"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "8: Wartortle"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "9: Blastoise"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "10: Caterpie"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "11: Metapod"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "12: Butterfree"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "13: Weedle"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "14: Kakuna"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "15: Beedrill"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "16: Pidgey"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "17: Pidgeotto"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "18: Pidgeot"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "19: Rattata"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "20: Raticate"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "21: Spearow"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "22: Fearow"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "23: Ekans"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "24: Arbok"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "25: Pikachu"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "26: Raichu"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "27: Sandshrew"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "28: Sandslash"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "29: Nidoran♀"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "30: Nidorina"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "31: Nidoqueen"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "32: Nidoran♂"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "33: Nidorino"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "34: Nidoking"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "35: Clefairy"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "36: Clefable"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "37: Vulpix"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "38: Ninetales"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "39: Jigglypuff"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "40: Wigglytuff"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "41: Zubat"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "42: Golbat"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "43: Oddish"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "44: Gloom"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "45: Vileplume"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "46: Paras"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "47: Parasect"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "48: Venonat"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "49: Venomoth"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "50: Diglett"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "51: Dugtrio"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "52: Meowth"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "53: Persian"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "54: Psyduck"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "55: Golduck"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "56: Mankey"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "57: Primeape"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "58: Growlithe"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "59: Arcanine"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "60: Poliwag"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "61: Poliwhirl"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "62: Poliwrath"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "63: Abra"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "64: Kadabra"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "65: Alakazam"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "66: Machop"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "67: Machoke"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "68: Machamp"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "69: Bellsprout"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "70: Weepinbell"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "71: Victreebel"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "72: Tentacool"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "73: Tentacruel"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "74: Geodude"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "75: Graveler"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "76: Golem"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "77: Ponyta"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "78: Rapidash"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "79: Slowpoke"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "80: Slowbro"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "81: Magnemite"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "82: Magneton"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "83: Farfetch'd"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "84: Doduo"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "85: Dodrio"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "86: Seel"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "87: Dewgong"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "88: Grimer"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "89: Muk"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "90: Shellder"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "91: Cloyster"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "92: Gastly"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "93: Haunter"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "94: Gengar"; source: "Gengar.qml"}
            ListElement { title: "95: Onix"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "96: Drowzee"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "97: Hypno"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "98: Krabby"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "99: Kingler"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "100: Voltorb"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "101: Electrode"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "102: Exeggcute"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "103: Exeggutor"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "104: Cubone"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "105: Marowak"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "106: Hitmonlee"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "107: Hitmonchan"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "108: Lickitung"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "109: Koffing"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "110: Wheezing"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "111: Rhyhorn"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "112: Rhydon"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "113: Chansey"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "114: Tangela"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "115: Kangaskhan"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "116: Horsea"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "117: Seadra"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "118: Goldeen"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "119: Seaking"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "120: Staryu"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "121: Starmie"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "122: Mr. Mine"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "123: Scyther"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "124: Jynx"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "125: Electabuzz"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "126: Magmar"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "127: Pinsir"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "128: Tauros"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "129: Magikarp"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "130: Gyarados"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "131: Lapras"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "132: Ditto"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "133: Eevee"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "134: Vaporeon"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "135: Jolteon"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "136: Flareon"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "137: Porygon"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "138: Omanyte"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "139: Omastar"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "140: Kabuto"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "141: Kabutops"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "142: Aerodactyl"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "143: Snorlax"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "144: Articuno"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "145: Zapdos"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "146: Moltres"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "147: Dratini"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "148: Dragonair"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "149: Dragonite"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "150: Mewtwo"; source: "../PageUnavailable/Pageunavailable.qml" }
            ListElement { title: "151: Mew"; source: "../PageUnavailable/Pageunavailable.qml" }
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }
 
 }
